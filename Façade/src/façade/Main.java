package façade;

public class Main {
    
    public static void main(String[] args) {
        
        int n1 = 3;
        int n2 = 2;
        
        Façade facade = new Façade();
        
        System.out.println(facade.realizarSoma(n1, n2));
        System.out.println(facade.realizarSubtracao(n1, n2));
        
    }
    
}
