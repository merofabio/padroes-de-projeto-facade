package façade;

public class Soma {

    public int somar(int x, int y) {
        System.out.println("Classe Soma: somar(" + x + "," + y + ")");
        return x + y;
    }

}
