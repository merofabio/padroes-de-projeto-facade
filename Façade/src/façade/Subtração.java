package façade;

public class Subtração {

    public int subtrair(int x, int y) {
        System.out.println("Classe Subtração: subtrair(" + x + "," + y + ")");
        return x - y;
    }

}
