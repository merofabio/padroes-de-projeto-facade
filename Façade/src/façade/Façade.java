package façade;

public class Façade {

    private Soma soma;
    private Subtração subtracao;
    
    public Façade() {
        
        this.soma = new Soma();
        this.subtracao = new Subtração();
        
    }
    
    public int realizarSoma(int x, int y) {
        System.out.println("Classe Façade: realizarSoma(" + x + "," + y + ")");
        return soma.somar(x, y);
    }

    public int realizarSubtracao(int x, int y) {
        System.out.println("Classe Façade: realizarSubtracao(" + x + "," + y + ")");
        return subtracao.subtrair(x, y);
    }

}
